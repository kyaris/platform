<?php
/**
 * Part of the VertexHS application.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the 3-clause BSD License.
 *
 * This source file is subject to the 3-clause BSD License that is
 * bundled with this package in the LICENSE file.  It is also available at
 * the following URL: http://www.opensource.org/licenses/BSD-3-Clause
 *
 * @package    VertexHS
 * @version    1.0.0
 * @author     VertexHS
 * @license    BSD License (3-clause)
 * @copyright  (c) 2011-2014, VertexHS LLC
 * @link       http://vertexhs.com
 */

if ( ! function_exists("array_filter_recursive"))
{
	function array_filter_recursive($array)
	{
		foreach ($array as $key => &$value)
		{
			if (empty($value))
			{
				unset($array[$key]);
			}
			else
			{
				if (is_array($value))
				{
					$value = array_filter_recursive($value);

					if (empty($value)) 
					{
						unset($array[$key]);
					}
				}
			}
		}

		return $array;
	}
}

if ( ! function_exists("format_date"))
{
	/**
	 * Uses Carbon to convert and format a string to a proper date.
	 * You can pass any string that is parsable by strtotime.
	 * 
	 * @param  string $string
	 * @param  string $format
	 * @param  string $timezone
	 * @return string
	 */
	function format_date($string, $format = "m/d/Y", $timezone = null)
	{
		if (empty($timezone))
		{
			if (Sentry::check() and $user = Sentry::getUser() and ! empty($user->timezone))
			{
				$timezone = $user->timezone;
			}
			else
			{
				$timezone = Config::get("vertexhs.timezone", Config::get("app.timezone"));
			}
		}

		$date = new Carbon\Carbon($string);

		return $date->setTimezone($timezone)->format($format);
	}
}

if ( ! function_exists("in_group"))
{
	function in_group($group, int $user = null)
	{
		try
		{
			if ( ! $user instanceof UserInterface)
			{
				if (is_numeric($user))
				{
					// Find the user using the user id
					$user = Sentry::findUserByID(1);
				}
				elseif (Sentry::check())
				{
					$user = Sentry::getUser();
				}
				else
				{
					dd($user);
				}
			}

		    // Check if the user is in the administrator group
		    if ($user->inGroup($group))
		    {
		        return true;
		    }
		    else
		    {
		        return false;
		    }
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
		    return false;
		}
		catch (Cartalyst\Sentry\Groups\GroupNotFoundException $e)
		{
		    return false;
		}
	}
}
