<?php namespace Kyaris\Platform\DataGrid\Handlers;
/**
 * Part of the Kyaris application.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the 3-clause BSD License.
 *
 * This source file is subject to the 3-clause BSD License that is
 * bundled with this package in the LICENSE file.  It is also available at
 * the following URL: http://www.opensource.org/licenses/BSD-3-Clause
 *
 * @package    Kyaris
 * @version    1.0.0
 * @author     Kyaris
 * @license    BSD License (3-clause)
 * @copyright  (c) 2011-2014, Kyaris LLC
 * @link       http://kyaris.com
 */

use Cartalyst\DataGrid\DataHandlers\DatabaseHandler;
use MongoInt64;

class MongoHandler extends DatabaseHandler {

	/**
	 * Applies a filter to the given query.
	 *
	 * @param  mixed   $query
	 * @param  string  $column
	 * @param  string  $operator
	 * @param  mixed   $value
	 * @param  string  $boolean
	 * @return void
	 */
	protected function applyFilter($query, $column, $operator, $value, $boolean = 'and')
	{
		$method = ($boolean === 'and') ? 'where' : 'orWhere';

		switch ($operator)
		{
			case 'like':
				$value = "%{$value}%";
				break;

			case 'regex':

				if ($this->supportsRegexFilters())
				{
					$method .= 'Raw';
				}

				switch ($connection = $this->getConnection())
				{
					case $connection instanceof MySqlDatabaseConnection:
						$query->$method("{$column} {$operator} ?", array($value));
						return;
				}

				return;
		}

		$query->$method($column, $operator, $value);
	}
}
